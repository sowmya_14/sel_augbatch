package testcase.GroupsAttribute;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003_FindLeads extends ProjectMethods {
@BeforeTest
public void setData() {
	testCaseName= "TCOO3_FindLeads";
	testCaseDesc="Find Lead";
	category="sanity";
	author="sowmya";
}
@Test(groups={"sanity"},dependsOnGroups= {"smoke"})
public void findlead() {
	WebElement eleLeads= locateElement("linkText","Leads");
	click(eleLeads);
	WebElement eleFind = locateElement("linkText","Find Leads");
	click(eleFind);
	WebElement elePhone = locateElement("linkText","Phone");
	click(elePhone);
	WebElement eleNumber = locateElement("name","phoneNumber");
	type(eleNumber, "9008007001");
	WebElement eleFindLeads =locateElement("xpath","//button[text()='Find Leads']");
	click(eleFindLeads);
	WebElement eleselect = locateElement("xpath","(//a[@class='linktext'])[4]");
	String select = eleselect.getText();
	click(eleselect);
	WebElement eledelete = locateElement("linkText","Delete");
	click(eledelete);
	 eleFind = locateElement("linkText","Find Leads");
	click(eleFind);
	WebElement elesearch = locateElement("name","id");
	type(elesearch, select);
	 eleFindLeads =locateElement("xpath","//button[text()='Find Leads']");
	click(eleFindLeads);
	
	
	
}
}
