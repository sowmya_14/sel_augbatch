package testcase.GroupsAttribute;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC004_EditLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
	testCaseName= "TCOO3_EditLead";
	testCaseDesc="Edit the created lead";
	category="sanity";
	author="sowmya";
	}
	@Test(groups = {"sanity"},dependsOnGroups= {"smoke"})
	public void editlead() {
		System.out.println("edit lead");
		
	}
}
