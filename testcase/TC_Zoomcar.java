package testcase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;


public class TC_Zoomcar extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName= "TC_Zoomcar";
		testCaseDesc="Book a car";
		category="smoke";
		author="sowmya";
	}
	@Test
	public void login() {
		startApp("chrome", "https://zoomcar.com/chennai");
		WebElement elestart = locateElement("xpath","//a[@title='Start your wonderful journey']");
		click(elestart);
		WebElement eleroute = locateElement("xpath","(//div[@class='items'])[1]");
		click(eleroute);
		WebElement elenext = locateElement("xpath","//button[text()='Next']");
		click(elenext);
		// get current date
		Date date = new Date();
		// get only the date(and not month, year,time etc)
		DateFormat sdf = new SimpleDateFormat("dd");
		//get today's date
		String today = sdf.format(date);
		//convert to integerand add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
		// print tmrw's date
		System.out.println(tomorrow);
		WebElement eletmrw = locateElement("xpath","//div[contains(text(),'"+tomorrow+"')]");
		String tmrwdate = eletmrw.getText();
		click(eletmrw);
		click(elenext);
		WebElement elestartdate = locateElement("xpath","//div[contains(text(),'"+tomorrow+"')]");
		String startdate = elestartdate.getText();
		if(tmrwdate.equals(startdate)) {
			WebElement eledone = locateElement("xpath","//button[text()='Done']");
			click(eledone);
		}
		List<WebElement> eleresult = driver.findElementsByXPath("//div[@class='img']");
		int size = eleresult.size();
		System.out.println("Total results"+size);
		//WebElement eleprice = locateElement("xpath","//div[text()=' Price: High to Low ']");
		//click(eleprice);
		int maxPrice = 0;
		String highestPricedCar = null;
		List<WebElement> elecarname = driver.findElementsByXPath("//div[@class='details']/h3");
		List<WebElement> elerate = driver.findElementsByXPath("//div[@class='price']");
		for (int i = 0; i < elecarname.size(); i++) {		
	
			String text = elerate.get(i).getText();
			int price = Integer.parseInt(text.replaceAll("\\D", ""));
			//System.out.println(price);
			
			if(price > maxPrice ) {
				maxPrice = price;
				highestPricedCar = elecarname.get(i).getText();
			}


		}
		
		System.out.println(maxPrice);
		System.out.println(highestPricedCar);

WebElement elebook = locateElement("xpath","(//div[contains(text(),'"+maxPrice+"')]/following::button)[1]");
click(elebook);
takeSnap();
System.out.println("booknow is clicked");
test.pass("The Book now of the maximum priced Car is clicked");

	}




}

