package week2.day2;

import java.util.Scanner;

import org.apache.poi.util.SystemOutLogger;

public class HwArthmeticOpswithUserInputs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner sc = new Scanner(System.in);
System.out.println("Enter the nos");
int n1 = sc.nextInt();
int n2 = sc.nextInt();
System.out.println("Enter your selection to perform the respective operation");	
System.out.println("1.Addition\t2.Subtraction\t3.Multiplication\t4.Division\t5.Modulus");
String Choice = sc.next();
switch (Choice) {
case "Addition":
	System.out.println("The Addition of " +n1+ "and" +n2 +"is :"+(n1+n2));
	break;
case "Subtraction":
	System.out.println("The Subtraction of" +n1+"and"+n2+ "is :"+(n1-n2));
	break;
case "Multiplication":
	System.out.println("The Multiplication of" +n1+"and"+n2+ "is :"+(n1*n2));
	break;
case "Division":
	System.out.println("The Division of" +n1+"and"+n2+ "is :"+(n1/n2));
	break;
case "Modulus":
	System.out.println("The Modulus of" +n1+"and"+n2+ "is :"+(n1%n2));
	break;
	default:
		System.out.println("Invalid Choice");
	
}
sc.close();
	}

}
