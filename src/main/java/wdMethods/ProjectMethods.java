package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import week6.day2.LearnReadEcel;

public class ProjectMethods extends SeMethods{
	@BeforeSuite(groups= {"common"})
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass(groups= {"common"})
	public void beforeClass() {
		startTestCase();
	}
	@Parameters({"url","username","password"})
	@BeforeMethod(groups= {"common"})
	
	public void login(String url, String username, String password) {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);
	}
	@AfterMethod(groups= {"common"})
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite(groups= {"common"})
	public void afterSuite() {
		endResult();
	}
	
	@DataProvider(name="positive")
	public Object[] [] fetchData() throws Exception{
	/*	Object[][] data = new Object[2][3];
		data[0][0]="testLeaf";
		data[0][1]="Sowmya";
		data[0][2]="E";
		
		data[1][0]="testLeaf";
		data[1][1]="Elangovan";
		data[1][2]="D";*/
		
		
		return LearnReadEcel.getexceldata(excelFileName);
		
	}
	
	
	
	
	
	
}
