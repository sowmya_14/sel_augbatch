package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class CwLearnExtentReporter {
	public static void main(String[] args) throws IOException {
ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
html.setAppendExisting(true);
ExtentReports extent = new ExtentReports();
extent.attachReporter(html);
// test case level
ExtentTest test = extent.createTest("TC001_CreateLead", "Create a new lead in leaftaps");

test.assignCategory("smoke");
test.assignAuthor("sowmya");
//tets case step level
test.pass("Browser launched succesfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
extent.flush();
	
	
	}
	
	
	
}