package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC005_DuplicateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
	testCaseName= "TCOO4_DeleteLead";
	testCaseDesc="delete the created lead";
	category="smoke";
	author="sowmya";
	}
	@Test(dataProvider="positive")
	public void deletelead(String email) {
		WebElement eleLeads= locateElement("linkText","Leads");
		click(eleLeads);
		WebElement eleFind = locateElement("linkText","Find Leads");
		click(eleFind);
		//WebElement elePhone = locateElement("linkText","Phone");
		//click(elePhone);
		WebElement eleNumber = locateElement("xpath","//span[text()='Email']");
		click(eleNumber);
		WebElement eleemail = locateElement("xpath","//input[@name='emailAddress']");
		type(eleemail, email);
		WebElement eleFindLeads =locateElement("xpath","//button[text()='Find Leads']");
		click(eleFindLeads);
		WebElement eleselect = locateElement("xpath","(//a[@class='linktext'])[4]");
		click(eleselect);
		WebElement eleedit = locateElement("xpath","//a[text()='Duplicate Lead']");
		click(eleedit);
		WebElement eletitle = locateElement("xpath","//div[text()='Duplicate Lead']");
		System.out.println(eletitle.getText());
		WebElement elecreate = locateElement("xpath","//input[@value='Create Lead']");
		click(elecreate);
	}
	
	@DataProvider(name="positive")
	public Object[][] fetchdata() {
		Object data[][] = new Object[3][1];
		data[0][0] = "abc@gmail.com";
		data[1][0]= "test@gmail.com";
		data[2][0]= "sda@gmail.com";
		
		return data;
		
	}
}

	
	

