package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC004_EditLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
	testCaseName= "TCOO3_EditLead";
	testCaseDesc="Edit the created lead";
	category="smoke";
	author="sowmya";
	}
	@Test(dataProvider= "true")
	public void editlead(String efname, String ecname) {
		WebElement eleLeads= locateElement("linkText","Leads");
		click(eleLeads);
		WebElement eleFind = locateElement("linkText","Find Leads");
		click(eleFind);
		WebElement elefname = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(elefname,efname);
		WebElement eleFindLeads =locateElement("xpath","//button[text()='Find Leads']");
		click(eleFindLeads);
		WebElement eleselect = locateElement("xpath","(//a[@class='linktext'])[4]");
		click(eleselect);
		WebElement eletitle = locateElement("id","sectionHeaderTitle_leads");
		String title = eletitle.getText();
		WebElement eleedit = locateElement("xpath","//a[text()='Edit']");
		click(eleedit);
		WebElement elecompany = locateElement("id","updateLeadForm_companyName");
		//elecompany.clear();
		type(elecompany,ecname);
		WebElement eleupdate = locateElement("xpath","//input[@value='Update']");
		String updatedvalue =eleupdate.getText();
		click(eleupdate);
		WebElement eleconfirm = locateElement("xpath","//span[contains(text(),'"+updatedvalue+"')]");
		System.out.println(eleconfirm.getText());
		
				
	}
	@DataProvider(name= "true")
	public Object[][] fetchdata(){
		Object data[][]=new Object[2][2];
		data[0][0] = "sarath";
		data[0][1] = "accenture";
		
		data[1][0] = "babu";
		data[1][1] = "CTS";
		return data;
		
	}
}
