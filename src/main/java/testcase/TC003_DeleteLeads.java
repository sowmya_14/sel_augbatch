package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003_DeleteLeads extends ProjectMethods {
@BeforeTest
public void setData() {
	testCaseName= "TCOO3_DeleteLeads";
	testCaseDesc="Find Lead";
	category="smoke";
	author="sowmya";
}
@Test(dataProvider ="true")
public void findlead(String phone) {
	WebElement eleLeads= locateElement("linkText","Leads");
	click(eleLeads);
	WebElement eleFind = locateElement("linkText","Find Leads");
	click(eleFind);
	WebElement elePhone = locateElement("linkText","Phone");
	click(elePhone);
	WebElement eleNumber = locateElement("name","phoneNumber");
	type(eleNumber, phone);
	WebElement eleFindLeads =locateElement("xpath","//button[text()='Find Leads']");
	click(eleFindLeads);
	WebElement eleselect = locateElement("xpath","(//a[@class='linktext'])[4]");
	String select = eleselect.getText();
	click(eleselect);
	WebElement eledelete = locateElement("linkText","Delete");
	click(eledelete);
	 eleFind = locateElement("linkText","Find Leads");
	click(eleFind);
	WebElement elesearch = locateElement("name","id");
	type(elesearch, select);
	 eleFindLeads =locateElement("xpath","//button[text()='Find Leads']");
	click(eleFindLeads);
	
	
	
}
@DataProvider(name="true")
public Object[][] fetchdata() {
	Object data[][] = new Object[2][1];
	data[0][0] = "236846565";
	data[1][0]= "21321323";
	
	
	return data;
	
}
}
