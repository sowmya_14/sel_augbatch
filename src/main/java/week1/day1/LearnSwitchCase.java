package week1.day1;

import java.util.Scanner;

public class LearnSwitchCase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the N1");
		int N1 = scan.nextInt();
		System.out.println("Enter the N2");
		int N2 = scan.nextInt();
		System.out.println("Choose your option");
		int option = scan.nextInt();
		switch (option) {
		case 1:
			System.out.println("Addition :" +(N1+N2));
			break;
		case 2:
			System.out.println("Sub :" +(N1-N2));
			break;
			default : System.out.println("Invalid choice");
		}
	}

}
