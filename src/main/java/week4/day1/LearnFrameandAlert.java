package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrameandAlert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// set third part binders
		
		
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		// implicit wait time
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// get the URL
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		driver.switchTo().alert().sendKeys("sowmya");
		driver.switchTo().alert().accept();
		String text = driver.findElementById("demo").getText();
		 System.out.println(text);
		
		
	}

}
