package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class HwMergeLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		// set third part bindersss
		
		
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		// implicit wait
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//get the URL
		driver.get("http://leaftaps.com/opentaps");
		// maximize the window
		driver.manage().window().maximize();
		// enter the user name
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//inspect the password
		driver.findElementById("password").sendKeys("crmsfa");
		//click to login
		driver.findElementByClassName("decorativeSubmit").click();
		//click on crmsfa
		driver.findElementByLinkText("CRM/SFA").click();
		// click on leads
		driver.findElementByLinkText("Leads").click();
		// click on Merge Leads
		driver.findElementByLinkText("Merge Leads").click();
		// click on Icon near from lead
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif']/parent::a)[1]").click();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> allwindows = new  ArrayList<String>();
		allwindows.addAll(windowHandles);
		driver.switchTo().window(allwindows.get(1));
		// enter the lead Id
				 driver.findElementByName("id").sendKeys("10280");
		// click on find leads button
				driver.findElementByXPath("//button[text()='Find Leads']").click();
				Thread.sleep(3000);
				// click on the first resulting result
				driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
				// move to primary window
				driver.switchTo().window(allwindows.get(0));
		// click  on Icon near to lead
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif']/parent::a)[2]").click();
		// move to new window
		 windowHandles = driver.getWindowHandles();
		 allwindows = new  ArrayList<String>();
		allwindows.addAll(windowHandles);
		driver.switchTo().window(allwindows.get(1));
		// enter the lead Id
		driver.findElementByName("id").sendKeys("10282");
// click on find leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		// click on the first resulting result
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		// move to primary window
		driver.switchTo().window(allwindows.get(0));
		// click Merge
		driver.findElementByLinkText("Merge").click();
		// accept alert
		driver.switchTo().alert().accept();
		// click find leads
		driver.findElementByLinkText("Find Leads").click();
		Thread.sleep(2000);
		// Enter from lead ID
		driver.findElementByXPath("(//input[@name='id'])").sendKeys("10280");
		// click on find leads button
				driver.findElementByXPath("//button[text()='Find Leads']").click();
				// verify the error message
				System.out.println(driver.findElementByXPath("//div[text()= 'No records to display']"));
				// close the browser
				driver.close();
		
	}

}
