package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnDragandDrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// set third part binders
		
		
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		// implicit wait time
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// get the URL
		driver.get("http://jqueryui.com/draggable/");
		driver.manage().window().maximize();
	
		// store the frame in an webelement and switch to frame
		WebElement frame= driver.findElementByClassName("demo-frame");
		driver.switchTo().frame(frame);
		// perform actions
		Actions builder = new Actions(driver);
		WebElement drag = driver.findElementById("draggable");
		//WebElement drop = driver.findElementById("droppable");
		System.out.println(drag.getLocation());
		builder.dragAndDropBy(drag, 250, 150).perform();;
		
		
		
		
		

	}

}
