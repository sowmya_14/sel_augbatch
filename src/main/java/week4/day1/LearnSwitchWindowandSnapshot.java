package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnSwitchWindowandSnapshot {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		// set third part binders
		
		
				System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
				ChromeDriver driver = new ChromeDriver();
				// implicit wait time
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				// get the URL
				driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
				driver.manage().window().maximize();
				//click on conactus
				driver.findElementByLinkText("Contact Us").click();;
				// store all the windows reference
				Set<String> windowHandles = driver.getWindowHandles();
				List<String> allwindows = new  ArrayList<String>();
				allwindows.addAll(windowHandles);
				driver.switchTo().window(allwindows.get(1));
				// get the title of second tab
				System.out.println(driver.getTitle());
				// get the url of second tab
				System.out.println(driver.getCurrentUrl());
				// take screenshot of current tab
				File src = driver.getScreenshotAs(OutputType.FILE);
				File desc = new File("./snaps/img.png");
				FileUtils.copyFile(src, desc);
				driver.switchTo().window(allwindows.get(0)).close();
				
				
	}

}
