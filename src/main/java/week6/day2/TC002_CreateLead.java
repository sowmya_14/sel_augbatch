package week6.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods {
	
	
	@BeforeTest(groups= {"common"})
	public void setData() {
		testCaseName= "TCOO2_CreateLead";
		testCaseDesc="Create a new Lead";
		category="smoke";
		author="sowmya";
		excelFileName = "CL";
	}
	
	@Test(groups= {"smoke"},dataProvider= "positive")	
	public void createLead(String cName,String fName,String lName) {
		WebElement eleLeads= locateElement("linkText","Leads");
		click(eleLeads);
		WebElement eleCreateLead =locateElement("linkText","Create Lead");
		click(eleCreateLead);
		WebElement eleCN = locateElement("id","createLeadForm_companyName");
		type(eleCN,cName);
		WebElement eleFN = locateElement("id","createLeadForm_firstName");
		type(eleFN,fName);
		WebElement eleLN = locateElement("id","createLeadForm_lastName");
		type(eleLN,lName);
		WebElement eleCreateLead1 = locateElement("name","submitButton");
		click(eleCreateLead1);

	}
	
	
}
