package week6.day2;

import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class LearnReadEcel {

	public static Object[][]getexceldata(String filename) throws IOException {
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+filename+".xlsx");
		// -- GET SHEET OF INDEX
		XSSFSheet sheet = wbook.getSheetAt(0);
		// -- To get the last Row num in a sheet
		int rowCount  = sheet.getLastRowNum();
		System.out.println("Row count = "+rowCount);
		int coulmnCount = sheet.getRow(0).getLastCellNum();
		System.out.println("coulmn count ="+coulmnCount);
		Object [] []data = new Object[rowCount][coulmnCount];
		for(int j =1;j<=rowCount;j++) {
			XSSFRow row = sheet.getRow(j);
			for(int i =0;i<coulmnCount;i++) {
				XSSFCell cell = row.getCell(i);
				
				// condition to check if the data type is string or int
				if(cell.getCellTypeEnum()==CellType.STRING) {
				 data[j-1][i] = cell.getStringCellValue();
				}else 	if(cell.getCellTypeEnum()==CellType.NUMERIC) {
					data[j-1][i] = ""+cell.getNumericCellValue();
			
			}
		}

	}
		return data;

	}
}