package week3.day2;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class HwFindDuplicatesInArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
/*Find repeated numbers in a array list of Number.classDeclare an integer array like
int[]a = {13,15,67,88,65,13,99,67,65,87,13}
The result should be - duplicated numbers are 13 67 65*/
int a[] =	{13,15,67,88,65,13,99,67,65,87,13,99,199,99,199};	
int n = a.length;
// to print the duplicated numbers only once, we need to use Array .sort, set concept or string's inbuilt functions

//option#1
for(int i =0;i<n;i++) {
	for(int j =i+1;j<n;j++) {
		if(a[j]==a[i]) {
			System.out.print(a[i]+ " ");
			break;
		}
	}
}
	System.out.println();	
	Set<Integer>dupNumbers = new LinkedHashSet<Integer>();
	for(int i =0;i<n;i++) {
		for(int j=i+1;j<n;j++) {
			if(a[j]==a[i]) {
				dupNumbers.add(a[i]);
				
			}
		}
	}
	System.out.print(dupNumbers);
	
	//Using Arrays sort and to print duplicated values only once
	//option#2
	Arrays.sort(a);;
	String repeatedNum=" ";
	for(int i=0;i<a.length-1;i++) {
		if(a[i]==a[i+1]) {
			if(!repeatedNum.equals(a[i]+" ")) {
				repeatedNum=a[i]+" ";
				System.out.print(repeatedNum);
			}
		}
	}
	System.out.println();
	// to print first element in the sorted array if it is duplicated
	if(a[0]==a[1]) {
		System.out.print(a[0]+ " ");
	}
	for(int i =1;i<a.length-1;i++) {
		if(a[i]==a[i+1]&&a[i]!=a[i-1]) {
			
			System.out.print(a[i]+" ");
		}
	}
	System.out.println();
		
		
		
		
		
		
		
		
	}

}
