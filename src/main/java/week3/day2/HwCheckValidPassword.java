package week3.day2;

import java.util.Scanner;

public class HwCheckValidPassword {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Write a prog to check if password is valid or not
		//1.password should have atleast 1- character
		//2. password have have only letters and digits
		//3. password should have atleast 2 digits and 2 letters
		//4. password should have atleast one capital letter
		
		System.out.println("Enter the password");
		Scanner sc= new Scanner(System.in);
		String password1= sc.next();
		
		if(isValid(password1)) {
			System.out.println("Valid Password");
		}
			else {
				System.out.println("Invalid Password");
			}
			
		}

	

	private static boolean isValid(String password1) {
		char c;
		int digitcount=0,lettercount=0,uppercase=0;
		if(password1.length()<10) {
			System.out.println("Password should have atleast 10 character");
			return false;
		} else {
			for (int i=0;i<password1.length()-1;i++) {
				c=password1.charAt(i);
				if(!Character.isLetterOrDigit(c)) {
					System.out.println("Password should have only letters and digits");
					return false;
				}
				if(Character.isDigit(c)) {
					digitcount++;
				} else if(Character.isUpperCase(c)) {
					uppercase++;
					lettercount++;
				}else if(Character.isLetter(c)) {
					lettercount++;
				}
			}
			
			if(digitcount<2) {
				System.out.println("Password should have atleast 2 digits");
				return false;
			}
			else if(lettercount<2) {
				System.out.println("Password should have atleast 2 letters");
				return false;
			}
				else if(uppercase<1) {
					System.out.println("Password should have atleast 1 capital letter");
					return false;
				}
			}
				
		return true;
		
	}

}
