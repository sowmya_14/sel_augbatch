package week3.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class HwOpenIrctcPage {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// set third part binders
		
		
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		// implicit wait time
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// get the URL
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		try {
		driver.findElementById("userRegistrationForm:userName").sendKeys("SelTest");
		driver.findElementById("userRegistrationForm:password").sendKeys("Abcd@sel123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Abcd@sel123");
		WebElement src = driver.findElementById("userRegistrationForm:securityQ");
		Select drop = new Select(src);
		drop.selectByVisibleText("Who was your Childhood hero?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Dad");
		WebElement lang= driver.findElementById("userRegistrationForm:prelan");
		Select drop1 = new Select(lang);
		drop1.selectByVisibleText("English");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Sowmya");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		WebElement day = driver.findElementById("userRegistrationForm:dobDay");
		Select drop3 = new Select (day);
		drop3.selectByVisibleText("14");
		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
		Select drop4 = new Select(month);
		drop4.selectByVisibleText("AUG");
		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select drop5 = new Select(year);
		drop5.selectByVisibleText("1993");
		WebElement occ = driver.findElementById("userRegistrationForm:occupation");
		Select drop6 = new Select(occ);
		drop6.selectByVisibleText("Private");
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select drop7 = new Select(country);
		drop7.selectByValue("94");
		driver.findElementById("userRegistrationForm:email").sendKeys("testleaf@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9940322649");
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select drop8 = new Select(nationality);
		drop8.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:address").sendKeys("Chennai");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600023",Keys.TAB);
		
			Thread.sleep(5000);
			
			WebElement city = driver.findElementById("userRegistrationForm:cityName");
			Select drop9 = new Select(city);
			drop9.selectByIndex(1);
			Thread.sleep(5000);
			WebElement postoffice = driver.findElementById("userRegistrationForm:postofficeName");
			Select drop10 = new Select(postoffice);
			drop10.selectByIndex(1);
			driver.findElementById("userRegistrationForm:landline").sendKeys("432135");
			driver.findElementById("userRegistrationForm:resAndOff:0").click();
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException();
			// TODO Auto-generated catch block
			
		}
		
		
		finally {
			driver.close();
		}
		
		
	}

}
