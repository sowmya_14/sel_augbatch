package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.management.RuntimeErrorException;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class OpenWebBrowser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// set third part binders
		
		
System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
ChromeDriver Driver = new ChromeDriver();
// implicit wait
Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//get the URL
Driver.get("http://leaftaps.com/opentaps");
// maximize the window
Driver.manage().window().maximize();
//inspect the username
try {
	Driver.findElementById("username").sendKeys("DemoSalesManager");
	//inspect the password
	Driver.findElementById("password").sendKeys("crmsfa");
	//click to login
	Driver.findElementByClassName("decorativeSubmit").click();
	//click on crmsfa
	Driver.findElementByLinkText("CRM/SFA").click();
	//click on create lead
	Driver.findElementByLinkText("Create Lead").click();
	//enter the company name
	Driver.findElementById("createLeadForm_companyName").sendKeys("Accenture pvt ltd");
	//enter the first name
	Driver.findElementById("createLeadForm_firstName").sendKeys("Sowmya");
	//enter the last name
	Driver.findElementById("createLeadForm_lastName").sendKeys("Elangovan");
	//select  the source
	// inspecting the source field
	WebElement src = Driver.findElementById("createLeadForm_dataSourceId");
	Select dropdown = new Select(src);
	// selecting the source by visible text menthod
	dropdown.selectByVisibleText("Direct Mail");
	//select marketing campaign
	WebElement mrkt = Driver.findElementById("createLeadForm_marketingCampaignId");
	Select opt = new Select(mrkt);
	List<WebElement>options = opt.getOptions();
	// get the total no.of options
	int size = options.size();
	// select last but one
	opt.selectByIndex(size-2);
	//click on create lead
	Driver.findElementByClassName("smallSubmit").click();
} catch (NoSuchElementException e) {
	e.printStackTrace();
	throw new RuntimeException();
	// TODO Auto-generated catch block
	
}

finally {
	Driver.close();
}






	}

}
