package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class HwSelectLastButOne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
int i = 0;
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		// implicit wait time
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// get the URL
		driver.get("http://testleaf.herokuapp.com/pages/Dropdown.html");
		driver.manage().window().maximize();
		// ientify the webelement
		WebElement sample = driver.findElementById("dropdown1");
		Select drop = new Select(sample);
		List<WebElement> alloption = drop.getOptions();
		int lastbutone = alloption.size()-2;
		
		for (WebElement eachopt : alloption) {
			if(i==lastbutone) {
				eachopt.click();
				
			}
			i++;
			
		}
	}

}
